﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelegateAndEventTest
{
    class Program
    {
        //定义一个委托
        public delegate void SaySomething(string name);
        //方法一
        public void SayHello(string name)
        {
            Console.WriteLine("Hello," + name + "!");
        }
        //方法二
        public void SayNiceToMeetYou(string name)
        {
            Console.WriteLine("Nice to meet you," + name + "!");
        }
        //定义一个事件---come这个事件只能和SaySomething 这个委托打交道，并且事件发生的时候会发消息给这个委托
        public event SaySomething come;
        //测试方法
        public void test()
        {
            //前两行是实例化委托
            SaySomething sayhello = new SaySomething(SayHello);
            SaySomething saynice = new SaySomething(SayNiceToMeetYou);
            //将委托加到事件上
            come += sayhello;
            come += saynice;
            //触发一个事件
            come("张三");

            #region 上面的简写
            ///因为在定义事件的时候已经关联了委托所以可以简写
            //come += SayHello;
            //come += SayNiceToMeetYou;
            //come("张三");
            #endregion
        }
        static void Main(string[] args)
        {
            Program program = new Program();
            program.test(); 
            Console.Read();
        }
    }

}
