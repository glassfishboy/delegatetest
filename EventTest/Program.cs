﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EventTest
{
    class Program
    {
        static void Main(string[] args)
        {
            TextClass textClass = new TextClass();

            // 将事件处理程序添加到事件的调用列表中(即事件布线)
            textClass.Changed += new TextClass.ChangedEventHandler(RespondChanged);

            string str = "";
            while (str != "quit")
            {
                Console.WriteLine("please enter a string:");
                str = Console.ReadLine();

                textClass.Text = str;
            }
        }

        // 对Change事件处理的程序
        private static void RespondChanged(object sender, EventArgs e)
        {
            Console.WriteLine("text has been changed  :{0}\n", ((TextClass)sender).Text);
        }
    }

    public class TextClass
    {
        // 定义事件的委托
        public delegate void ChangedEventHandler(object sender, EventArgs e);

        // 定义一个事件
        public event ChangedEventHandler Changed;

        // 用以触发Change事件
        protected virtual void OnChanged(EventArgs e)
        {
            if (this.Changed != null)
                this.Changed(this, e);
        }

        #region 属性
        private string _text = "";
        // Text属性
        public string Text
        {
            get { return this._text; }
            set
            {
                this._text = value;
                // 文本改变时触发Change事件
                this.OnChanged(new EventArgs());
            }
        }
        #endregion
    }

}
