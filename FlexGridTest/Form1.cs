﻿using System;
using System.Windows.Forms;
using C1.Win.C1FlexGrid;

namespace FlexGridTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Show OutlineBar on column 0.
            c1FlexGrid1.Tree.Column = 0;
            c1FlexGrid1.Tree.Style = TreeStyleFlags.Symbols;

            // Clear existing subtotals.
            c1FlexGrid1.Subtotal(AggregateEnum.Clear);

            // Get a Grand total (use -1 instead of column index).
            c1FlexGrid1.Subtotal(AggregateEnum.Sum, -1, -1, 3, "Grand Total");

            // Total per Product (column 0).
            c1FlexGrid1.Subtotal(AggregateEnum.Sum, 0, 0, 3, "Total {0}");

            // Total per Region (column 1).
            c1FlexGrid1.Subtotal(AggregateEnum.Sum, 1, 1, 3, "Total {0}");

            // Size column widths based on content.
            c1FlexGrid1.AutoSizeCols();
 
        }
    }
}
