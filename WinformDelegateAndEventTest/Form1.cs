﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinformDelegateAndEventTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("我被点击了！");

            #region 自己实现事件绑定
            #region 第一种实现
            //ShowName sn = new ShowName(ShowButtonName);
            //btnclick += sn;
            #endregion

            #region 第二种实现
            btnclick += ShowButtonName;
            #endregion
            
            btnclick(sender);
            #endregion
        }

        public delegate void ShowName(object sender);
        private event ShowName btnclick;

        private void ShowButtonName(object sender) {
            Button btn = sender as Button;
            if (btn!=null)
            {
                MessageBox.Show("按钮名称是"+btn.Text);
            }
        }
    }
}
